//
//  APIManager.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//


import Foundation
import Moya

// MARK: - Provider setup

let placesProvider = MoyaProvider<GoogleAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])

// MARK: - Provider support

public enum GoogleAPI {
    case places(keyword: String, formattedLocation: String)
}

extension GoogleAPI: TargetType {
    public var baseURL: URL { return URL(string: "https://maps.googleapis.com/maps/api/place/")! }
    
    public var path: String {
        switch self {
        case .places:
            return "nearbysearch/json"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var task: Task {
        switch self {
        case .places(let keyword, let formattedLocation):
            return .requestParameters(parameters: ["location": formattedLocation,
                                                   "type": APIDefaultParameters.type.rawValue,
                                                   "key": APIConstants.googleKey.keyName,
                                                   "radius": APIDefaultParameters.radius.rawValue,
                                                   "keyword": keyword,
                                                   "fields" : APIDefaultParameters.fields.rawValue],
                                      encoding: URLEncoding.default)
        }
    }
    
    public var validationType: ValidationType {
        switch self {
        case .places:
            return .successCodes
        }
    }
    
    public var sampleData: Data {
        switch self {
        case .places:
            return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
        }
    }
    
    public var headers: [String: String]? {
        return nil
    }
}
