//
//  PlacesService.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit

typealias SuccesCompletion = ([Place]) -> ()
typealias FailureCompletion = () -> ()

@objc class PlacesService: NSObject {

    @objc public func getPlaces(with keyword: String, latitude: Double, longitude: Double, success: @escaping SuccesCompletion, failure: @escaping FailureCompletion) {
        let formattedLocation = "\(latitude),\(longitude)"
        
        placesProvider.request(.places(keyword: keyword, formattedLocation: formattedLocation)) { result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                do {
                    let decoder = JSONDecoder()
                    let placeResponse = try decoder.decode(APIResponse.self, from: data)
                    if let places = placeResponse.results, places.count > 0 {
                        success(places)
                    } else {
                        failure()
                    }
                } catch _ {
                    //TODO: - maybe pass the error in the future
                    failure()
                }
                
            case .failure(_):
                //TODO: - maybe pass the error in the future
                failure()
            }
        }
    }
}
