//
//  MainRouter.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit

@objc class MainRouter: NSObject {
    @objc static func showPlaces(_ places: [Place], on vc: UIViewController?) {
        let assembler = PlacesAssembler(places: places)
        let placesVC = assembler.assemble()
        self.showVC(placesVC, on: vc)
    }
}

private extension MainRouter {
    private static func showVC(_ vc: UIViewController,
                               on currentVC: UIViewController?) {
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
}
