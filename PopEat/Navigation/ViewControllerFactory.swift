//
//  ViewControllerFactory.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit

@objc class ViewControllerFactory: NSObject {
    private static var placesVCIdentifier: String {
        return String(describing: PlacesViewController.self)
    }

    static func placesViewController(_ cellsViewModels: [PlacesCellViewModel], mapViewModel: PlacesMapViewModel) -> PlacesViewController {
        //force unwrap is acceptable because I want the app to crash in order to find the problem quickly
        guard let viewController = UIStoryboard(.places).instantiateViewController(withIdentifier: placesVCIdentifier) as? PlacesViewController else { fatalError() }
        
        viewController.cellsViewModels = cellsViewModels
        viewController.mapViewModel = mapViewModel
        
        return viewController
    }
}
