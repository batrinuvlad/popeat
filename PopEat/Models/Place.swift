//
//  Place.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit

class APIResponse: NSObject, Codable {
    var status: String?
    var results: [Place]?
}

class Place: NSObject, Codable {
    var id: String?
    var name: String?
    var iconUrl: URL?
    var address: String?
    var types: [String]?
    var geometry: Geometry?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case iconUrl = "icon"
        case address = "vicinity"
        case types
        case geometry
    }
}

class Geometry: NSObject, Codable {
    var location: Location?
}

class Location: NSObject, Codable {
    var lat: Double?
    var long: Double?
    
    private enum CodingKeys: String, CodingKey {
        case lat
        case long = "lng"
    }
}
