//
//  LoadingView.h
//  PopEat
//
//  Created by Vlad Batrinu on 1/26/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import "LoadingView.h"

#define WEAKIFY(object) __typeof__(object) __weak weakSelf = object
#define kHydeLoadingViewTag         123456789
#define kRemoveAnimationDuration    0.3

@interface LoadingView ()

@property (nonatomic, assign) NSInteger appearanceCount;
@property (nonatomic, assign) BOOL isRemoving;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alignYSpinnerConstraint;

@end

@implementation LoadingView {
    @private
    __weak IBOutlet UILabel *_loadingTextLabel;
    UIView *_alphaLayerView;
}

#pragma mark - Singleton

+(instancetype)sharedInstance {
    static LoadingView *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LoadingView class]) owner:nil options:nil] firstObject];
    });

    return sharedInstance;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.tag = kHydeLoadingViewTag;
    }
    return self;
}


#pragma mark - Public 

-(void)showLoadingViewSofly {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)showLoadingView {
    WEAKIFY(self);
    
    [self ensureIsMainThreadWithCompletion:^{
        [weakSelf showLoadingViewAction];
    }];
}

-(void)showLoadingViewWithText:(NSString*)text {
    WEAKIFY(self);
    [self ensureIsMainThreadWithCompletion:^{
        [weakSelf showLoadingViewActionWithText:text];
    }];
}

-(void)removeLoadingView {
    WEAKIFY(self);
    [self ensureIsMainThreadWithCompletion:^{ 
        [weakSelf removeLoadingViewAction];
    }];
}


#pragma mark - Private Utils

-(void)ensureIsMainThreadWithCompletion:(void(^)(void))completion {
    if([NSThread isMainThread]) {
        completion();
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion();
        });
    }
}

-(void)showLoadingViewAction {
    _loadingTextLabel.text = @"";
    self.alignYSpinnerConstraint.constant = 0.0f;
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    UIView *viewWithTag = [window viewWithTag:kHydeLoadingViewTag];
    if (viewWithTag && !self.isRemoving) {
        self.appearanceCount ++;
        return;
    }
    
    if(self.isRemoving) {
        self.isRemoving = NO;
        [_alphaLayerView removeFromSuperview];
        [self removeFromSuperview];
    }
    
    _alphaLayerView = [[UIView alloc] initWithFrame:window.frame];
    _alphaLayerView.backgroundColor = [UIColor clearColor];
    _alphaLayerView.userInteractionEnabled = self.isModal;
    
    self.center = window.center;
    self.alpha = 0.0f;
    _alphaLayerView.alpha = 0.0f;
    
    [window addSubview:_alphaLayerView];
    [window addSubview:self];
    
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 1.0f;
        self->_alphaLayerView.alpha = 1.0f;
    } completion:nil];
    
}

-(void)showLoadingViewActionWithText:(NSString*)text {
    [self showLoadingViewAction];
    _loadingTextLabel.text = text;
    self.alignYSpinnerConstraint.constant = 12;
}

-(void)removeLoadingViewAction {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (self.appearanceCount == 0) {

        self.isRemoving = YES;
        [UIView animateWithDuration:kRemoveAnimationDuration animations:^{
            self.alpha = 0.0f;
            self->_alphaLayerView.alpha = 0.0f;
        }
        completion:^(BOOL finished) {

            if(self.isRemoving) {
                [self->_alphaLayerView removeFromSuperview];
                [self removeFromSuperview];
                self.isRemoving = NO;
            }
        }];
    }
    else {
        self.appearanceCount--;
    }
}

-(UIWindow *)topMostWindow
{
    for (UIWindow *window in [[UIApplication sharedApplication] windows].reverseObjectEnumerator) {
        if( !window.hidden ){
            return window;
        }
    }
    return [[[UIApplication sharedApplication] windows] firstObject];
}

@end
