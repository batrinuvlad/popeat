//
//  LoadingView.h
//  PopEat
//
//  Created by Vlad Batrinu on 1/26/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import <UIKit/UIKit.h>

#define __LOADING                        [[LoadingView sharedInstance] showLoadingView]
#define __LOADING_SOFT                   [[LoadingView sharedInstance] showLoadingViewSofly]
#define __LOADING_TEXT(__TEXT__)         [[LoadingView sharedInstance] showLoadingViewWithText:__TEXT__]
#define __REMOVELOADING                  [[LoadingView sharedInstance] removeLoadingView]

@interface LoadingView : UIView

+(instancetype)sharedInstance;

@property (assign, nonatomic) BOOL isModal;

-(void)showLoadingView;
-(void)showLoadingViewSofly;
-(void)showLoadingViewWithText:(NSString* )text;
-(void)removeLoadingView;

@end
