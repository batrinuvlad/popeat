//
//  LocationManager.m
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#define _enforceTrue(bool) if((bool) == NO) return;
#define _enforceFalse(bool) if((bool) == YES) return;

#import "LocationManager.h"

@interface LocationManager()

@property (nonatomic, strong) LocationManager *sharedInstance;
/**
 *  Needs to be strong retained property, not ivar.
 */
@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic, assign) BOOL locationModeAlways;
@property (nonatomic, assign) BOOL requestedOnlyAccess;

@end

@implementation LocationManager

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    static LocationManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [LocationManager new];
    });
    
    return _sharedInstance;
}

#pragma mark - Custom getters

-(CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
    }
    
    return _locationManager;
}

-(BOOL)isLocationAuthorized {
    BOOL locationPermitted = NO;
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        locationPermitted = YES;
    }
    
    return locationPermitted;
}

#pragma mark - Public methods

-(void)requestLocationPermissionsModeAlways:(BOOL)locationModeAlways {
    // If the app already has access to the requested mode, return.
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == (locationModeAlways ? kCLAuthorizationStatusAuthorizedAlways : kCLAuthorizationStatusAuthorizedWhenInUse)) {
        [self delegateRequestPermissionsStatus:YES];
        return;
    } else if (status == kCLAuthorizationStatusDenied) {
        [self delegateRequestPermissionsStatus:NO];
        return;
    }
    
    self.locationModeAlways = locationModeAlways;
    self.requestedOnlyAccess = YES;
    [self requestLocationAccess];
}

-(void)requestLocationAlways:(BOOL)locationModeAlways {
    self.locationModeAlways = locationModeAlways;
    self.requestedOnlyAccess = NO;
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    [self decideBasedOnAuthorizationStatus:status];
}

#pragma mark - Utils

-(void)decideBasedOnAuthorizationStatus:(CLAuthorizationStatus)status {
    BOOL failed = NO;
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            // First time asking.
            [self requestLocationAccess]; // Handle response in delegate methods.
            break;
        case kCLAuthorizationStatusRestricted:
            // App restricted. User can't change status. Abort.
            failed = YES;
            break;
        case kCLAuthorizationStatusDenied: {
            // The user explicitly denied the use of location services for this app OR
            //  location services are currently disabled in Settings. Abort.
            failed = YES;
            break;
        }
        case kCLAuthorizationStatusAuthorizedAlways:
            // This case is also used in iOS 7 for permissions granted.
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            if (self.requestedOnlyAccess) {
                [self delegateRequestPermissionsStatus:YES];
            } else {
                [self.locationManager startUpdatingLocation];
            }
            break;
        }
        default:
            break;
    }
    
    if (failed) {
        if (self.requestedOnlyAccess) {
            [self delegateRequestPermissionsStatus:NO];
        } else {
            SEL failedUpdate = @selector(locatingFailedWithStatus:);
            _enforceTrue([self checkDelegateForSelector:failedUpdate]);
            
            FailedStatus theStatus = (status == kCLAuthorizationStatusDenied ? FailedStatusDenied : FailedStatusRestricted);
            [self.delegate locatingFailedWithStatus:theStatus];
        }
        [self.locationManager stopUpdatingLocation];
    }
}

-(void)requestLocationAccess {
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        if (self.locationModeAlways) {
            [self.locationManager requestAlwaysAuthorization];
        } else {
            [self.locationManager requestWhenInUseAuthorization];
        }
    } else {
        [self.locationManager startUpdatingLocation];
    }
}

-(BOOL)checkDelegateForSelector:(SEL)selector {
    if (!self.delegate) {
        NSLog(@"No delegate is set.");
        return NO;
    }
    if(![self.delegate respondsToSelector:selector]) {
        NSLog(@"Delegate doesn't implement selector: %@", NSStringFromSelector(selector));
        return NO;
    }
    
    return YES;
}

-(void)delegateRequestPermissionsStatus:(BOOL)status {
    SEL successRequest = @selector(locationPermissionsUpdatedWithStatus:);
    _enforceTrue([self checkDelegateForSelector:successRequest]);
    [self.delegate locationPermissionsUpdatedWithStatus:status];
}

#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [self decideBasedOnAuthorizationStatus:status];
}

-(void)locationManager:(CLLocationManager *)manager
    didUpdateLocations:(NSArray *)locations {
    _enforceFalse(self.requestedOnlyAccess);
    
    CLLocation *lastLocation = [locations lastObject];
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;

    SEL successUpdate = @selector(locatingSuccessWithLatitude:andLongitude:);
    _enforceTrue([self checkDelegateForSelector:successUpdate]);
    [self.delegate locatingSuccessWithLatitude:lastLocation.coordinate.latitude
                                  andLongitude:lastLocation.coordinate.longitude];
}

-(void)locationManager:(CLLocationManager *)manager
      didFailWithError:(NSError *)error {
    BOOL resetLocationManager = YES;
    BOOL failed = NO;
    
    switch (error.code) {
        case kCLErrorLocationUnknown:
            // The location manager was unable to obtain a location value right now.
            resetLocationManager = NO;
            break;
        case kCLErrorDenied:
            // Access to the location service was denied by the user.
            resetLocationManager = NO;
            // We handle this case in the statusUpdated method.
            //failed = YES;
            break;
        case kCLErrorNetwork:
            // The network was unavailable or a network error occurred.
            // Retry.
            break;
        case kCLErrorHeadingFailure:
            // The heading could not be determined.
            // Retry;
            break;
        case kCLErrorDeferredFailed:
            // If you get this error on a device that has GPS hardware, the solution is to try again.
            // Retry.
            break;
        case kCLErrorDeferredNotUpdatingLocation:
            // The location manager did not enter deferred mode because location updates were already disabled or paused.
            // Retry;
            break;
        case kCLErrorDeferredAccuracyTooLow:
            // Deferred mode is not supported for the requested accuracy. The accuracy must be set to kCLLocationAccuracyBest or kCLLocationAccuracyBestForNavigation.
            // Retry.
            manager.desiredAccuracy = kCLLocationAccuracyBest;
            break;
        case kCLErrorDeferredDistanceFiltered:
            // Deferred mode does not support distance filters. Set the distance filter to kCLDistanceFilterNone.
            // Retry.
            manager.distanceFilter = kCLDistanceFilterNone;
            break;
        case kCLErrorDeferredCanceled:
            resetLocationManager = NO;
        default:
            break;
    }
    
    if (failed) {
        SEL failedUpdate = @selector(locatingFailedWithStatus:);
        _enforceTrue([self checkDelegateForSelector:failedUpdate]);
        
        FailedStatus status = (error.code == kCLErrorDenied ? FailedStatusDenied : FailedStatusRestricted);
        [self.delegate locatingFailedWithStatus:status];
    }
    if (resetLocationManager) {
        [self.locationManager startUpdatingLocation];
    } else {
        [self.locationManager stopUpdatingLocation];
    }
}

@end
