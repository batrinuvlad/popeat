//
//  LocationManager.h
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import CoreLocation;

#define LocationM [LocationManager sharedInstance]

typedef enum : NSUInteger {
  FailedStatusRestricted = 0,
  FailedStatusDenied
} FailedStatus;

@protocol LocationManagerDelegate;

@interface LocationManager : NSObject <CLLocationManagerDelegate, UIAlertViewDelegate>

+(instancetype _Nullable )sharedInstance;

-(void)requestLocationPermissionsModeAlways:(BOOL)locationModeAlways;
-(void)requestLocationAlways:(BOOL)locationModeAlways;

@property (nonatomic, strong, nullable) id<LocationManagerDelegate> delegate;
@property (nonatomic, readonly) BOOL isLocationAuthorized;

@end

@protocol LocationManagerDelegate <NSObject>

@optional

-(void)locatingFailedWithStatus:(FailedStatus)status;
-(void)locatingSuccessWithLatitude:(double)latitude
                      andLongitude:(double)longitude;
-(void)locationPermissionsUpdatedWithStatus:(BOOL)accepted;

@end
