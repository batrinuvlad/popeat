//
//  SearchVC.m
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import "SearchVC.h"
#import "LoadingView.h"
#import "LocationManager.h"
#import "UIFont+Extension.h"
#import "UIColor+Extension.h"
#import "PopEat-Swift.h"


const NSString * SEARCHED_TYPE = @"Restaurant";

@interface SearchVC () <LocationManagerDelegate, UISearchBarDelegate>

@property (nonatomic, strong, nullable) CLLocation *currentLocation;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (nonatomic) CGFloat centerHorizontalSearchOffset;

@end

@implementation SearchVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initialSetup];
    [self setupLocationManager];
    [self setupTapOutsideGestures];
    [self setupNavigationBar];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self animateLabelToAppear];
    [self setupSearchBar];
}

-(void)initialSetup {
    self.imageView.image = [UIImage imageNamed:@"illustration"];
    self.descriptionLabel.text = [NSString stringWithFormat:@"Search for your favorite %@", SEARCHED_TYPE];
    self.descriptionLabel.font = [UIFont sfUITextRegularWithSize:24];
    self.descriptionLabel.textColor = [UIColor searchDescriptionLabelColor];
    self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
    self.descriptionLabel.numberOfLines = 0;
}

-(void)animateLabelToAppear {
    [UIView animateWithDuration:1 animations:^{
        self.descriptionLabel.alpha = 1;
    }];
}

-(void)animateLabelToDisappear {
    [UIView animateWithDuration:1 animations:^{
        self.descriptionLabel.alpha = 0;
    }];
}

-(void)rotateImageView {
    CABasicAnimation *rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(360 * M_PI / 180.0f)];
    rotation.duration = 1;
    rotation.repeatCount = HUGE_VALF;
    
    [self.imageView.layer addAnimation:rotation forKey:@"Spin"];
}

-(void)stopRotation {
    [self.imageView.layer removeAllAnimations];
}

-(void)getPlacesForKeyword:(NSString *)keyword withCompletion:(void (^)(NSArray<Place *> *places))block {
    if (self.currentLocation == nil) { return; }
    [self rotateImageView];
    __LOADING;
    [self animateLabelToDisappear];
    PlacesService *placesService = [PlacesService new];
    [placesService getPlacesWith:keyword latitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude success:^(NSArray<Place *> *places) {
        [self stopRotation];
        __REMOVELOADING;
        block(places);
    } failure:^{
        [self stopRotation];
        [self animateLabelToAppear];
        __REMOVELOADING;
    }];
}

-(void)setupNavigationBar {
    [self setupNavigationItemTitleView];
    [self setupNavigationBarTranslucency];
}

-(void)setupNavigationItemTitleView {
    UIImage *image = [UIImage imageNamed:@"logo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = imageView;
}

-(void)setupNavigationBarTranslucency {
    UIImage *blankImage = [UIImage new];
    [self.navigationController.navigationBar setBackgroundImage:blankImage forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = blankImage;
    [self.navigationController.navigationBar setTranslucent: YES];
}

-(void)setupTapOutsideGestures {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)setupSearchBar {
    [self customizeSearchBar];
    self.searchBar.delegate = self;
}

-(void)customizeSearchBar {
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    
    CGFloat padding = 28;
    CGFloat searchBarWidth = self.view.frame.size.width - padding;
    CGFloat placeholderIconWidth = searchField.leftView.frame.size.width;
    CGFloat placeholderWidth = searchField.attributedPlaceholder.size.width;
    CGFloat offsetIconToPlaceholder = 8;
    
    if (placeholderIconWidth) {
        CGFloat placeholderWithIcon = placeholderIconWidth + offsetIconToPlaceholder + placeholderWidth;
        CGFloat centerHorizontal = searchBarWidth / 2 - placeholderWithIcon / 2;
        self.centerHorizontalSearchOffset = centerHorizontal;
        [self centerSearchTextField];
    }
}

-(void)setupLocationManager {
    if (LocationM != nil) {
        LocationM.delegate = self;
        [LocationM requestLocationAlways:NO];
    }
}

#pragma mark Location Delegate

- (void)locatingSuccessWithLatitude:(double)latitude andLongitude:(double)longitude {
    self.currentLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
}

-(void)locatingFailedWithStatus:(FailedStatus)status {
    // TODO: - in the future display error
}

#pragma mark Search Delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self resetSearchBarOnInitialPosition];
    
    if (searchBar.text == nil) { return; }
    [self getPlacesForKeyword:searchBar.text withCompletion:^(NSArray<Place *> *places) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MainRouter showPlaces:places on:self];
        });
    }];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self resetSearchBarOnInitialPosition];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self centerSearchTextField];
}

#pragma mark Private Utils

-(void)resetSearchBarOnInitialPosition {
    UIOffset offset = UIOffsetMake(0, 0);
    [self.searchBar setPositionAdjustment:offset forSearchBarIcon:UISearchBarIconSearch];
}

-(void)centerSearchTextField {
    UIOffset offset = UIOffsetMake(self.centerHorizontalSearchOffset, 0);
    [self.searchBar setPositionAdjustment:offset forSearchBarIcon:UISearchBarIconSearch];
}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}

@end
