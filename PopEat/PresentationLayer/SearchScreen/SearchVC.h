//
//  SearchVC.h
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Place;
@class PlacesService;

NS_ASSUME_NONNULL_BEGIN

@interface SearchVC : UIViewController

@end

NS_ASSUME_NONNULL_END
