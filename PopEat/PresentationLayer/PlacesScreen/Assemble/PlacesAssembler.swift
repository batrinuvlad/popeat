//
//  PlacesAssembler.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import Foundation

struct PlacesAssembler {
    let places: [Place]
    
    init(places: [Place]) {
        self.places = places
    }
    
    func assemble() -> PlacesViewController {
        let viewModelsFactory = PlacesViewModelFactory(places: places)
        let cellsViewModels = viewModelsFactory.createCellsViewModels()
        let mapViewModel = viewModelsFactory.createMapViewModel()
        
        let viewController = ViewControllerFactory.placesViewController(cellsViewModels, mapViewModel: mapViewModel)
        return viewController
    }
}
