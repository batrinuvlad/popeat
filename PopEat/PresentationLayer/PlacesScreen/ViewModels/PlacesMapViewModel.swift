//
//  PlacesMapViewModel.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import Foundation

struct PlacesMapViewModel {
    var places: [Place]
    
    init(places: [Place]) {
        self.places = places
    }

    var firstPlaceViewModel: PlaceViewModel? {
        guard let firstPlace = places.first else { return nil }
        return PlaceViewModel(place: firstPlace)
    }
    
    var nextPlacesViewModels: [PlaceViewModel] {
        return places.dropFirst().map { return PlaceViewModel(place: $0) }
    }
    
    func placeId(at index: Int) -> String? {
        guard places.indices.contains(index) else { return nil }
        guard let placeId = places[index].id else { return nil }
        return placeId
    }
    
    var firstLocation: CLLocation? {
        guard let lat = places.first?.geometry?.location?.lat, let long = places.first?.geometry?.location?.long else { return nil }
        return CLLocation(latitude: lat, longitude: long)
    }
    
    func cameraViewModel(for location: CLLocation) -> CameraSettingsViewModel {
        return CameraSettingsViewModel(lat: location.coordinate.latitude, long: location.coordinate.longitude, zoom: 15)
    }
    
    func nextClosestLocation(to newPosition: CLLocation) -> CLLocation? {
        let positionsArray = places.compactMap({ place -> CLLocation? in
            guard let lat = place.geometry?.location?.lat, let long = place.geometry?.location?.long else { return nil }
            return CLLocation(latitude: lat, longitude: long)
        })
        
        return positionsArray.min(by:
            { $0.distance(from: newPosition) < $1.distance(from: newPosition) })
    }
}

struct PlaceViewModel {
    var place: Place
    
    init(place: Place) {
        self.place = place
    }
    
    var position: CLLocationCoordinate2D? {
        guard let lat = place.geometry?.location?.lat, let long = place.geometry?.location?.long else { return nil }
        return CLLocationCoordinate2DMake(lat, long)
    }
    
    var id: String? { return place.id }
    var name: String? { return place.name }
    var shouldAnimate: Bool { return true }
    var shouldTrackViewChanges: Bool { return true }
}

struct CameraSettingsViewModel {
    var lat: CLLocationDegrees
    var long: CLLocationDegrees
    var zoom: Float
    
    init(lat: CLLocationDegrees, long: CLLocationDegrees, zoom: Float) {
        self.lat = lat
        self.long = long
        self.zoom = zoom
    }
}
