//
//  PlacesCellViewModel.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit

class PlacesCellViewModel {
    var place: Place
    
    init(place: Place) {
        self.place = place
    }
    
    var imageUrl: URL? { return place.iconUrl }
    var name: String? { return place.name }
    var mainType: String? { return place.types?.first?.capitalized.replacingOccurrences(of: "_", with: " ") }
    var displayedAddress: String? {
        let addressParts = place.address?.split(separator: ",")
        guard let address = addressParts?.first else { return nil }
        return String(address)
    }
}
