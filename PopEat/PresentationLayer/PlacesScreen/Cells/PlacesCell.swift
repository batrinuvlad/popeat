//
//  PlacesCell.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit
import SDWebImage

class PlacesCell: UITableViewCell {
    var viewModel: PlacesCellViewModel!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var roundView: RoundView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
        setupLabels()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            roundView.backgroundColor = UIColor.selectedCell()
        } else {
            roundView.backgroundColor = UIColor.imageBackground()
        }
    }
    
    func initialSetup() {
        backgroundColor = UIColor.clear
        selectionStyle = .none
    }
    
    func setupLabels() {
        setupLabelColors()
        setupLabelFonts()
    }
    
    func setupLabelColors() {
        nameLabel.textColor = UIColor.titleLabel()
        typeLabel.textColor = UIColor.titleLabel()
        addressLabel.textColor = UIColor.descriptionLabel()
        
    }

    func setupLabelFonts() {
        nameLabel.font = UIFont.sfUITextBold(withSize: 20)
        typeLabel.font = UIFont.sfUITextMedium(withSize: 14)
        addressLabel.font = UIFont.sfUITextRegular(withSize: 14)
    }
    
    func setup(with viewModel: PlacesCellViewModel) {
        self.viewModel = viewModel
        
        iconImageView.sd_setImage(with: viewModel.imageUrl, completed: nil)
        iconImageView.contentMode = .scaleAspectFit
        nameLabel.text = viewModel.name
        typeLabel.text = viewModel.mainType
        addressLabel.text = viewModel.displayedAddress
    }
}
