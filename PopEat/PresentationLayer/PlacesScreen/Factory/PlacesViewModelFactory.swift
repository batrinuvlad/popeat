//
//  PlacesViewModelFactory.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit

struct PlacesViewModelFactory {
    var places: [Place]
    
    init(places: [Place]) {
        self.places = places
    }
    
    func createCellsViewModels() -> [PlacesCellViewModel] {
        return places.map( {PlacesCellViewModel(place: $0)} )
    }
    
    func createMapViewModel() -> PlacesMapViewModel {
        return PlacesMapViewModel(places: places)
    }
}
