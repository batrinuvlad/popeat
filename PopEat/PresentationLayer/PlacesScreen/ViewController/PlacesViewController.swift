//
//  PlacesViewController.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit
import GoogleMaps

class PlacesViewController: UIViewController {
    
    // MARK: - MapView
    @IBOutlet weak var mapView: GMSMapView!
    var markers: [GMSMarker] = [GMSMarker]()
    var selectedMarker: GMSMarker?
    var placeMarkerDictionary = [String: GMSMarker]()
    var markerIndexDicionary = [String: Int]()
    
    // MARK: - View Models
    var cellsViewModels: [PlacesCellViewModel] = [PlacesCellViewModel]()
    var mapViewModel: PlacesMapViewModel!
    
    // MARK: - TableView
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMap()
        setupTableView()
        setupNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
        
        selectFirstRow()
    }
    
    func setupNavigationBar() {
        title = "Nearby places"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back-button"),
                                                           style: .plain,
                                                           target: navigationController,
                                                           action: #selector(UINavigationController.popViewController(animated:)))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.navigationTitle()
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.navigationTitle(),
                                                                   .font: UIFont.sfUITextMedium(withSize: 18)]
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.register(PlacesCell.self)
    }
    
    private func dropShadowOnMap() {
        mapView.layer.masksToBounds = false
        mapView.layer.shadowColor = UIColor.black.cgColor
        mapView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        mapView.layer.shadowRadius = 4
        mapView.layer.shadowOpacity = 0.5
    }
    
    func setupMap() {
        guard let firstLocation = mapViewModel.firstLocation else { return }
        
        dropShadowOnMap()
        mapView.delegate = self
        updateCamera(with: firstLocation)
        displayPins()
    }
    
    func displayPins() {
        guard let firstPlaceViewModel = mapViewModel.firstPlaceViewModel else { return }
        
        displayFirstPin(placeViewModel: firstPlaceViewModel)
        displayOtherPins()
    }
    
    func displayFirstPin(placeViewModel: PlaceViewModel) {
        guard let id = placeViewModel.id else { return }
        
        if let firstMarker = marker(for: placeViewModel, with: #imageLiteral(resourceName: "glow-marker")) {
            selectedMarker = firstMarker
            markers.append(firstMarker)
            placeMarkerDictionary[id] = firstMarker
            let key = "\(firstMarker.position.latitude)\(firstMarker.position.longitude)"
            markerIndexDicionary[key] = 0
        }
    }
    
    func displayOtherPins() {
        mapViewModel.nextPlacesViewModels.enumerated().forEach({ (index, placeViewModel) in
            guard let marker = self.marker(for: placeViewModel, with: #imageLiteral(resourceName: "arrow")) else { return }
            markers.append(marker)
            let key = "\(marker.position.latitude)\(marker.position.longitude)"
            markerIndexDicionary[key] = index + 1
            if let id = placeViewModel.id {
                placeMarkerDictionary[id] = marker
            }
        })
    }
    
    func marker(for placeViewModel: PlaceViewModel, with image: UIImage) -> GMSMarker? {
        guard let position = placeViewModel.position else { return nil }
        let marker = GMSMarker()
        
        marker.position = position
        marker.tracksViewChanges = placeViewModel.shouldTrackViewChanges ? true : false
        marker.appearAnimation = placeViewModel.shouldAnimate ? .pop : .none

        //uncomment to test easier, even though it's not in the design
        //marker.snippet = placeViewModel.name
        marker.map = mapView
        marker.icon = image
        
        return marker
    }
    
    func findClosest(to newPosition: CLLocation) -> GMSMarker? {
        guard let closest = mapViewModel.nextClosestLocation(to: newPosition) else { return nil }
        updateCamera(with: closest)
        
        return markers.filter { (marker) -> Bool in
            return marker.position.latitude == closest.coordinate.latitude && marker.position.longitude == closest.coordinate.longitude
        }.first
    }
    
    func selectClosest(_ closestMarker: GMSMarker) {
        selectMarker(closestMarker)
        selectRow(for: closestMarker)
    }
    
    func selectMarker(_ marker: GMSMarker) {
        selectedMarker?.icon = #imageLiteral(resourceName: "arrow")
        marker.icon = #imageLiteral(resourceName: "glow-marker")
        selectedMarker = marker
    }
    
    func selectRow(for marker: GMSMarker) {
        let key = "\(String(describing: marker.position.latitude))\(marker.position.longitude)"
        if let position = markerIndexDicionary[key] {
            tableView.selectRow(at: IndexPath(row: position, section: 0), animated: true, scrollPosition: .top)
        }
    }
    
    func updateCamera(with newLocation: CLLocation) {
        let cameraViewModel = mapViewModel.cameraViewModel(for: newLocation)
        let camera = GMSCameraPosition.camera(withLatitude: cameraViewModel.lat,
                                              longitude: cameraViewModel.long,
                                              zoom: cameraViewModel.zoom)
        mapView.animate(to: camera)
    }
    
    func selectFirstRow() {
         tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
    }
}

extension PlacesViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let location = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        if let closestMarker = findClosest(to: location) {
            selectClosest(closestMarker)
        }
    }
}

extension PlacesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PlacesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let viewModel = cellsViewModels[indexPath.row]
        cell.setup(with: viewModel)

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsViewModels.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let placeId = mapViewModel.placeId(at: indexPath.row), let selectedMarker = placeMarkerDictionary[placeId] else { return }
        mapView.selectedMarker = selectedMarker
        mapView.animate(toLocation: selectedMarker.position)
    }
}
