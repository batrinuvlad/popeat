//
//  UIFont+Extension.h
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (Extension)

+ (UIFont *)sfUITextRegularWithSize:(CGFloat)size;
+ (UIFont *)sfUITextMediumWithSize:(CGFloat)size;
+ (UIFont *)sfUITextBoldWithSize:(CGFloat)size;

@end

NS_ASSUME_NONNULL_END
