//
//  UIColor+Extension.m
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import "UIColor+Extension.h"

#define RGBColor(r, g, b, a) [UIColor colorWithRed:(float)r / 255.0 green:(float)g / 255.0 blue:(float)b / 255.0 alpha:(float)a]

@implementation UIColor (Extension)

+ (UIColor *)titleLabelColor {
    return RGBColor(38, 38, 38, 1);
}

+ (UIColor *)descriptionLabelColor {
    return RGBColor(102, 102, 102, 1);
}

+ (UIColor *)ovalBackgroundColor {
    return RGBColor(237, 237, 237, 1);
}

+ (UIColor *)shadowColor {
    return RGBColor(202, 202, 202, 1);
}

+ (UIColor *)imageBackgroundColor {
    return RGBColor(127, 185, 170, 1);
}

+ (UIColor *)navigationTitleColor {
    return RGBColor(217, 27, 68, 1);
}

+ (UIColor *)searchDescriptionLabelColor {
    return RGBColor(153, 153, 153, 1);
}

+ (UIColor *)selectedCellColor {
    return RGBColor(217, 27, 68, 1);
}

@end
