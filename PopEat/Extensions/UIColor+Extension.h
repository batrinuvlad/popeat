//
//  UIColor+Extension.h
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Extension)

+ (UIColor *)titleLabelColor;
+ (UIColor *)descriptionLabelColor;
+ (UIColor *)ovalBackgroundColor;
+ (UIColor *)shadowColor;
+ (UIColor *)imageBackgroundColor;
+ (UIColor *)navigationTitleColor;
+ (UIColor *)searchDescriptionLabelColor;
+ (UIColor *)selectedCellColor;
@end

NS_ASSUME_NONNULL_END
