//
//  UIFont+Extension.m
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import "UIFont+Extension.h"

@implementation UIFont (Extension)

+ (UIFont *)sfUITextRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+ (UIFont *)sfUITextMediumWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Medium" size:size];
}

+ (UIFont *)sfUITextBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Bold" size:size];
}

@end
