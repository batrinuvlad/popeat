//
//  Storyboard+Extension.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/24/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import Foundation

extension UIStoryboard {
    enum StoryboardType: String {
        case places
    }
    
    convenience init(_ storyboardType: StoryboardType) {
        let storyboardRawName = storyboardType.rawValue
        let storyboardName = storyboardType.rawValue.prefix(1).uppercased() + storyboardRawName.dropFirst()
        self.init(name: storyboardName, bundle: nil)
    }
}
