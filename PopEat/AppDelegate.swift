//
//  AppDelegate.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/23/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        GMSServices.provideAPIKey(APIConstants.googleKey.keyName)
        return true
    }
}

