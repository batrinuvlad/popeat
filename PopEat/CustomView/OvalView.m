//
//  OvalView.m
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import "OvalView.h"
#import "UIColor+Extension.h"

@implementation OvalView

- (void)layoutSubviews {
    [super layoutSubviews];

    [self setupGradient];
    [self setupCornerRadius];
    [self setupShadow];
}

-(void)setupGradient {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 1);
    gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor ovalBackgroundColor].CGColor];
    [self.layer insertSublayer:gradient atIndex:0];
}

-(void)setupCornerRadius {
    self.layer.cornerRadius = 40;
    self.clipsToBounds = YES;
    self.layer.masksToBounds = YES;
}

-(void)setupShadow {
    self.layer.shadowRadius  = 40;
    self.layer.shadowColor   = [UIColor shadowColor].CGColor;
    self.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    self.layer.shadowOpacity = 1.0f;
}

@end
