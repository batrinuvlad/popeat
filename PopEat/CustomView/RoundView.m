//
//  RoundImageView.m
//  PopEat
//
//  Created by Vlad Batrinu on 1/25/20.
//  Copyright © 2020 devinext. All rights reserved.
//

#import "RoundView.h"
#import "UIColor+Extension.h"

@implementation RoundView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.bounds.size.height / 2;
    self.clipsToBounds = YES;
    self.layer.masksToBounds = YES;
    self.contentMode = UIViewContentModeScaleToFill;
    self.backgroundColor = [UIColor imageBackgroundColor];
}

@end
