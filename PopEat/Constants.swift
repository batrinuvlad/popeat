//
//  Constants.swift
//  PopEat
//
//  Created by Vlad Batrinu on 1/27/20.
//  Copyright © 2020 devinext. All rights reserved.
//

enum APIConstants: String {
    case googleKey = "AIzaSyBXZ1NruIxTfLIlYU8t8dyAP0DzgDAh2uY"
    
    var keyName: String {
        return rawValue
    }
}

enum APIDefaultParameters: String {
    case type = "restaurant"
    case radius = "1500"
    case fields = "icon, name"
}
