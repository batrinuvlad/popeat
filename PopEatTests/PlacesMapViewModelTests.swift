//
//  MapViewModelTests.swift
//  PopEatTests
//
//  Created by Vlad Batrinu on 1/26/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import XCTest
@testable import PopEat

class PlacesMapViewModelTests: XCTestCase {
    private var sut: PlacesMapViewModel!
    
    override func setUp() {
        sut = PlacesMapViewModel(places: createMockPlacesArrayCorrect())
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_firstLocationInitialCorrectSetup_displaysLatAndLong() {
        XCTAssertNotNil(sut.firstLocation)
        XCTAssertEqual(sut.firstLocation!.coordinate.latitude, 37.9838)
        XCTAssertEqual(sut.firstLocation!.coordinate.longitude, 23.7275)
    }
    
    func test_firstLocationInitialIncorectSetup_safeUnwraps() {
        sut = PlacesMapViewModel(places: createMockPlacesArrayBroken())
        XCTAssertNil(sut.firstLocation)
    }
    
    func test_placeIdWithSameCorrectIndex_isSameOrder() {
        let elements = createMockPlacesArrayCorrect()
        let secondIdExpectation = elements[1].id
        
        sut = PlacesMapViewModel(places: elements)
        let secondIdFromViewModel = sut.placeId(at: 1)
        XCTAssertEqual(secondIdFromViewModel, secondIdExpectation)
    }
    
    func test_placeIdWithDifferentCorrectIndex_isNotSameOrder() {
        let elements = createMockPlacesArrayCorrect()
        let secondIdExpectation = elements[1].id
        
        sut = PlacesMapViewModel(places: elements)
        let secondIdFromViewModel = sut.placeId(at: 0)
        XCTAssertNotEqual(secondIdFromViewModel, secondIdExpectation)
    }
    
    func test_placeIdWithOutOfBoundsIndex_isNil() {
        let placeIdOverbounds = sut.placeId(at: 6)
        XCTAssertNil(placeIdOverbounds)
        
        let placeIdUndebounds = sut.placeId(at: -1)
        XCTAssertNil(placeIdUndebounds)
    }
    
    func test_closestLocation_isCorrectLocation() {
        sut.places[0].geometry?.location = createMockLocation1()
        sut.places[1].geometry?.location = createMockLocation2()
        sut.places[2].geometry?.location = createMockLocation3()
        sut.places[3].geometry?.location = createMockLocation4()
        
        let searchedLocation = CLLocation(latitude: 38.5000, longitude: 24.7275)
        let expectedToBeClosestLocation = sut.places[1].geometry!.location
        
        let closestPlaceReturned = sut.nextClosestLocation(to: searchedLocation)
        XCTAssertEqual(closestPlaceReturned?.coordinate.latitude, expectedToBeClosestLocation?.lat)
        XCTAssertEqual(closestPlaceReturned?.coordinate.longitude, expectedToBeClosestLocation?.long)
    }
}
