//
//  Mocks.swift
//  PopEatTests
//
//  Created by Vlad Batrinu on 1/26/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import Foundation
@testable import PopEat

func createPlaceMockRestaurant() -> Place {
    let place = Place()
    place.id = UUID().uuidString
    place.address = "str. Sf Lazar, nr 51, Iasi, Romania"
    place.geometry = createMockGeometry()
    place.types = ["bar", "restaurant", "burgers"]
    
    return place
}

func createPlaceEmpty() -> Place {
    let place = Place()
    
    return place
}

func createPlaceMockBookStore() -> Place {
    let place = Place()
    place.id = UUID().uuidString
    place.address = "str. Palace, nr 81, Iasi, Romania"
    place.name = "Best library"
    place.geometry = createMockGeometry()
    place.types = ["books", "library"]
    
    return place
}

func createMockPlacesArrayBroken() -> [Place] {
    return [createPlaceEmpty(), createPlaceEmpty()]
}

func createMockPlacesArrayCorrect() -> [Place] {
    return [createPlaceMockRestaurant(), createPlaceMockBookStore(), createPlaceMockRestaurant(), createPlaceMockRestaurant()]
}

func createMockGeometry() -> Geometry {
    let geometry = Geometry()
    geometry.location = createMockLocation1()
    return geometry
}

func createMockLocation1() -> Location {
    let location = Location()
    location.lat = 37.9838
    location.long = 23.7275
    return location
}

func createMockLocation2() -> Location {
    let location = Location()
    location.lat = 38.9838
    location.long = 24.7275
    return location
}

func createMockLocation3() -> Location {
    let location = Location()
    location.lat = 40.9838
    location.long = 26.7275
    return location
}

func createMockLocation4() -> Location {
    let location = Location()
    location.lat = 44.9838
    location.long = 26.7275
    return location
}
