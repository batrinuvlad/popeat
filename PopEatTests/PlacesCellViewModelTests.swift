//
//  PlacesCellViewModelTests.swift
//  PopEatTests
//
//  Created by Vlad Batrinu on 1/26/20.
//  Copyright © 2020 devinext. All rights reserved.
//

import XCTest
@testable import PopEat

class PlacesCellViewModelTests: XCTestCase {
    private var sut: PlacesCellViewModel!
    
    override func setUp() {
        sut = PlacesCellViewModel(place: createPlaceMockRestaurant())
    }
    
    func test_initialSetupWithAddress_isDisplayedCorrectly() {
        XCTAssertFalse((sut.displayedAddress?.contains(","))!)
        XCTAssertEqual(sut.displayedAddress?.split(separator: ",").count, 1)
    }
    
    func test_initialSetupWithEmpty_isNil() {
        sut.place = createPlaceEmpty()
        XCTAssertNil(sut.imageUrl)
        XCTAssertNil(sut.name)
        XCTAssertNil(sut.mainType)
        XCTAssertNil(sut.displayedAddress)
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

}
